class Notice {
  final String name;
  final String img;
  final String text;

  Notice(this.name, this.img, this.text);
}