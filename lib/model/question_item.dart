class QuestionItem{

  // Q&A 게시판 복수 R 리스트 보여줄 양식 폼.

  num id;
  DateTime questionCreateDate;
  String questionTitle;

  QuestionItem(this.id, this.questionCreateDate, this.questionTitle);

}