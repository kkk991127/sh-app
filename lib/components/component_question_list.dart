import 'package:dclost_app/model/question_%20form.dart';
import 'package:flutter/material.dart';

class ComponentQuestionList extends StatelessWidget {
  const ComponentQuestionList({
    super.key,
    required this.questionForm,
    required this.callback
  });

  final QuestionForm questionForm ;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Colors.black12,
                  ),
                ),
                child: Row(
                  children: [
                    Container(
                      width: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(
                            width: 1,
                            color: Colors.black12,
                          ),
                        ),
                      ),
                      child: Text(
                        '${questionForm.id}',
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 100,
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(
                            width: 1,
                            color: Colors.black12,
                          ),
                        ),
                      ),
                      child: Text('${questionForm.questionCreateDate.year}-${questionForm.questionCreateDate.month}-${questionForm.questionCreateDate.day}'),
                    ),
                    Container(
                      child: Text(
                        questionForm.questionTitle,
                      ),
                    ),
                  ],
                ),

               ),
            ],

          ),

        ),
      ),
    );
  }
}
