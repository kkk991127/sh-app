import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/components/component_item_info.dart';
import 'package:dclost_app/model/codi_detail.dart';
import 'package:dclost_app/pages/page_goods_detail.dart';
import 'package:flutter/material.dart';

class PageMagazine extends StatefulWidget {
  const PageMagazine({super.key});

  @override
  State<PageMagazine> createState() => _PageMagazineState();
}

// 매거진 페이지. 하단 리스트는 임의로 메인과 동일한 리스트 넣어놓은 값.

class _PageMagazineState extends State<PageMagazine> {
  // 임의의 리스트 데이터로 백엔드 연동 시 리스트 변경 메서드 발동 필요.

  List<CodiDetail> _list = [
    CodiDetail(1, 'assets/catalog1.png', '키치 코디 세트', 39000, 'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    CodiDetail(2, 'assets/catalog2.png', '스포티 코디 세트', 29000, 'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    CodiDetail(3, 'assets/catalog3.png', '페미닌 코디 세트', 39000, 'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    CodiDetail(4, 'assets/catalog4.png', '트위드 코디 세트', 59000, 'assets/info1.png', 'assets/info2.png', 'assets/info3.png')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                '" 디클로젯이 읽어주는 최신 패션 트렌드 "',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            Container(
              child: Image.asset('assets/magazine1.png'),
            ),
            Container(
              width: 450,
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
              ),
              child: Container(
                margin: EdgeInsets.all(20),
                width: 400,
                child: Column(
                  children: [

                    Text(
                      'Quiet Luxury 두고두고 간직하고 싶은 고요함 2023 패션 트렌드는 그 어떤 해보다 다채롭게 물들여졌다. 그 속에서 대신할 수 없는 존재감으로 사람들의 시선을 독식하고 있는 트렌드가 있었으니 은밀하고 고요하게 퍼져나간 콰이어트 럭셔리. 불과 몇년전까지만 해도 럭셔리라고 한다면 전방 500m에서도 한눈에 들어오는 브랜드 로고와 디자인을 미덕으로 삼았다. Long Black Coat 음미해봐야 알 수 있는 진가. 잘 건진 롱 블랙 코트 하나 열 재킷 부럽지 않을테니. 그리고 블랙 코트의 존재감은 작년보다 올해 더 뜨거울 것이다. 만능에 가까울 정도로 활용도 최상을 자랑하지만 시크함까지 겸비했으니 말이다.',
                      style: TextStyle(
                        letterSpacing: 2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 40, 0, 20),
              child: Column(
                children: [
                  Text(
                    '" 디클로젯 큐레이터가 추천해주는 최신 패션 코디 "',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 200.0,
              child: GridView.builder(
                  itemCount: _list.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    childAspectRatio: 3/4,
                  ), itemBuilder: (BuildContext ctx, int idx) {
                return ComponentItemInfo(codiDetail: _list[idx], callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(codiDetail: _list[idx])));
                }
                );
              }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
