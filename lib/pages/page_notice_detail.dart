import 'package:dclost_app/pages/page_notice.dart';
import 'package:flutter/material.dart';
import '../model/notice_item.dart';

class PageNoticeDetail extends StatefulWidget {
  const PageNoticeDetail({super.key, required this.notice});

  final Notice notice;

  @override
  State<PageNoticeDetail> createState() => _PageNoticeDetailState();
}
class _PageNoticeDetailState extends State<PageNoticeDetail> {

  /// 공지사항 디테일 페이지 입니다.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.notice.name),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
                height: 300,
                width: 300,
                decoration: BoxDecoration(border: Border.all(color: Colors.black,width: 1)),
                child: Image.asset(widget.notice.img)
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                '' + widget.notice.text,
              style: TextStyle(
                  fontSize: 14
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  //엘리베이터 버튼 스타일
                  minimumSize: Size(150, 50), // 버튼 사이즈 조절
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNotice()));
                }, child: Text('목록'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}