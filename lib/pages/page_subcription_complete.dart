import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/pages/codi_list.dart';
import 'package:flutter/material.dart';

class PageSubcriptionComplete extends StatelessWidget {
  const PageSubcriptionComplete({super.key});

  // 멤버십 가입 신청 완료 페이지

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 200, 0, 10),
              child: Text(
                '구독신청이 완료되었습니다.',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Container(
              child: Text(
                  '감사합니다.',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Text(
                  '지금 디클로젯의 서비스를 구경해보세요.',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                        child: ElevatedButton(
                          onPressed: () {
                          },
                          child: Text('매거진 보러 가기'),
                          style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                            backgroundColor: Colors.black,
                            foregroundColor: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(70 , 0, 0, 0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => CodiList()));
                          },
                          child: Text('카탈로그 보러 가기'),
                          style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                            backgroundColor: Colors.black,
                            foregroundColor: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),

    );
  }
}
