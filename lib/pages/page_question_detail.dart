import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/model/question_%20form.dart';
import 'package:dclost_app/pages/page_question_detail_correct_write.dart';
import 'package:flutter/material.dart';

class PageQuestionDetail extends StatefulWidget {
  const PageQuestionDetail({
    super.key,
    required this.questionForm
  });

  final QuestionForm questionForm;

  // Q&A 게시판 단수 R 자세히 보기 페이지
  @override
  State<PageQuestionDetail> createState() => _PageQuestionDetailState();
}

class _PageQuestionDetailState extends State<PageQuestionDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 500,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                  color: Colors.black12,
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    width: 500,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: Colors.black12,
                        ),
                      ),
                      child: Text(
                          widget.questionForm.questionTitle,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    width: 500,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.black12,
                      ),
                    ),
                    child: Text('${widget.questionForm.questionCreateDate.year}-${widget.questionForm.questionCreateDate.month}-${widget.questionForm.questionCreateDate.day}'),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    width: 500,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.black12,
                      ),
                    ),
                    child: Text( widget.questionForm.questionMemberId),
                  ),
                  Container(
                    width: 500,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.black12,
                      ),
                    ),
                    child: Text('내용'),
                  ),
                  Container(
                    width: 500,
                    height: 500,
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Text(widget.questionForm.questionContent),
                  ),

                      ],
                    )
                  ),
            Container(
              margin: EdgeInsets.fromLTRB(0 , 30, 0, 0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('목록'),
                style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageQuestionDetailCorrectWrite()));
                },
                style: ElevatedButton.styleFrom(
                  //엘리베이터 버튼 스타일
                  minimumSize: Size(70 ,40), // 버튼 사이즈 조절
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                ),
                child: Text('수정하기'),
              ),
            ),
                ],
              ),
            )
    );
  }
}
